Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '9.0'
s.name = "DadosAbertosBCB"
s.summary = "DadosAbertos Banco Central do Brasil."
s.requires_arc = true

# 2
s.version = "1.0.0"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4
s.author = { "Jessica Barros" => "jessica.jbbarros@gmail.com" }


s.homepage = "https://gitlab.com/jessica.jbbarros/DadosAbertosBCB"

s.swift_version = "4.2"

s.source = { :git => "https://gitlab.com/jessica.jbbarros/DadosAbertosBCB.git", :tag => "#{s.version}"}


# 7
s.framework = "UIKit"

# 8
s.source_files = "DadosAbertosBCB/**/*.{swift}"

end
