//
//  TRTaxService.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 02/10/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

/// Service to fetch the TR tax values in the BCB open data
public class TRTaxService {

    /// Monthly TR tax API URL
    private static var serverUrl = "https://api.bcb.gov.br"

    private static let endPoint = "/dados/serie/bcdata.sgs.4174/dados?formato=json"

    /// Change the server for use webdservice
    public static func setServer(_ url: String) {
        serverUrl = url
    }

    /// Function that seeks the monthly values of the TR tax
    ///
    /// - Parameter completion: function receiving the monthly TR tax
    public static func allMonthlyTRTax(completion: @escaping ([MonthlyTRTax], ErrorDadosAbertos?) -> Void) {

        guard let url = URL(string: serverUrl + endPoint) else {
            completion([], ErrorDadosAbertos.tryToReach)
            return
        }

        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in

            var trs: [MonthlyTRTax] = []

            guard let data = data, error == nil else { return }

            do {
                let newData = try JSONSerialization.jsonObject(with: data,
                                                               options: JSONSerialization.ReadingOptions.allowFragments)

                if let newData = newData as? [[String: Any]] {

                    for json in newData {

                        if let dateString = json["data"] as? String {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd/MM/yyyy"
                            if let date = dateFormatter.date(from: dateString) {
                                if let valueString = json["valor"] as? String {
                                    if let value = Float(valueString) {
                                        let taxa = MonthlyTRTax(date: date, value: value)
                                        trs.append(taxa)
                                    }
                                }
                            }
                        }
                    }
                    completion(trs, nil)
                }
            } catch {
                completion([], ErrorDadosAbertos.tryToReach)
            }
        }
        task.resume()
    }
}
