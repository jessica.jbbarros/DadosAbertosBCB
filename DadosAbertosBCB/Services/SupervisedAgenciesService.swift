//
//  AgenciasSupervisionadasService.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 20/08/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

/// Get all supervised agencies in Brazil
public class SupervisedAgenciesService {
    private static let url = URL(string:
        "https://olinda.bcb.gov.br/olinda/servico/Informes_Agencias/versao/v1/odata/Agencias?$top=100&$format=json")

    /// Get all supervised agencies in Brazil
    ///
    /// - Parameter completion: Function that will be called when data arrives
    public static func getAgencies(completion: @escaping ([Agency], ErrorDadosAbertos?) -> Void) {

        guard let url = url else {
            completion([], ErrorDadosAbertos.tryToReach)
            return
        }

        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in

            var agencies: [Agency] = []

            guard let data = data, error == nil else { return }
            var errorDadosAbertos: ErrorDadosAbertos?

            do {
                let newData = try JSONSerialization.jsonObject(with: data,
                                                               options: JSONSerialization.ReadingOptions.allowFragments)

                if let newData = newData as? [String: Any] {

                    if let agenciesList = newData["value"] as? [[String: Any]] {
                        for agencyJson in agenciesList {
                            agencies.append(parseJsonToAgency(json: agencyJson))
                        }
                    } else {
                        errorDadosAbertos = ErrorDadosAbertos.tryToParseJson
                    }
                } else {
                    errorDadosAbertos = ErrorDadosAbertos.tryToParseJson
                }
            } catch {
                errorDadosAbertos = ErrorDadosAbertos.tryToParseJson
            }

            completion(agencies, errorDadosAbertos)
        }

        task.resume()

    }

    /// Parse the income Json to an Angency
    ///
    /// - Parameter json: Json to be parsed
    /// - Returns: The Agency parsed from Json
    // swiftlint:disable:next cyclomatic_complexity
    private static func parseJsonToAgency(json: [String: Any]) -> Agency {
        var agency = Agency()
        if let cnpjBase = json["CnpjBase"] as? String {
            agency.cnpjBase = cnpjBase
        }
        if let cnpjSequencial = json["CnpjSequencial"] as? String {
            agency.cnpjSequencial = cnpjSequencial
        }
        if let cnpjDv = json["CnpjDv"] as? String {
            agency.cnpjDv = cnpjDv
        }
        if let nameIf = json["NomeIf"] as? String {
            agency.nameIf = nameIf
        }
        if let segment = json["Segmento"] as? String {
            agency.segment = segment
        }
        if let codeCompe = json["CodigoCompe"] as? String {
            agency.codeCompe = codeCompe
        }
        if let agencyName = json["NomeAgencia"] as? String {
            agency.agencyName = agencyName
        }
        if let adress = json["Endereco"] as? String {
            agency.adress = adress
        }
        if let number = json["Numero"] as? String {
            agency.number = number
        }
        if let complement = json["Complemento"] as? String {
            agency.complement = complement
        }
        if let neightborhood = json["Bairro"] as? String {
            agency.neightboord = neightborhood
        }
        if let cep = json["Cep"] as? String {
            agency.cep = cep
        }
        if let cityIbge = json["MunicipioIbge"] as? String {
            agency.cityIbge = cityIbge
        }
        if let city = json["Municipio"] as? String {
            agency.city = city
        }
        if let federativeUnit = json["UF"] as? String {
            agency.federativeUnit = federativeUnit
        }
        if let initialDate = json["DataInicio"] as? String {
            agency.initialDate = initialDate
        }
        if let DDD = json["DDD"] as? String {
            agency.DDD = DDD
        }
        if let phone = json["Telefone"] as? String {
            agency.phone = phone
        }
        return agency
    }
}
