//
//  RankingInstituicoesService.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 20/08/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

/// Get the Rank of Institutions in Brazil
public class RankingInstitutionsService {

    private static let urlString = "https://www3.bcb.gov.br/rdrweb/rest/ext/ranking/arquivo"

    /// Get the Rank of Institutions in Brazil
    ///
    /// - Parameters:
    ///   - year: The year when the rank was reliased
    ///   - periodicity: The periocity count in a year
    ///   - period: With group of periodicity to get
    ///   - type: The type of the institution to get
    ///   - completion: A scaping function to get the institutions
    public static func getRanking(year: String, periodicity: Periodicity, period: Int,
                                  type: RankType, completion: @escaping ([Institution], ErrorDadosAbertos?) -> Void) {
        var newUrlString = String(urlString)

        newUrlString.append(
            "?ano=\(year)&periodicidade=\(periodicity.rawValue)&periodo=\(period)&tipo=\(type.rawValue)")

        let urlTry = URL(string: newUrlString)

        var errorDadosAbertos: ErrorDadosAbertos?

        guard let url = urlTry else {
            errorDadosAbertos = ErrorDadosAbertos.tryToReach
            completion([], errorDadosAbertos)
            return
        }

        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in

            var institutions: [Institution] = []

            guard let data = data, error == nil else {
                errorDadosAbertos = ErrorDadosAbertos.tryToReach
                completion([], errorDadosAbertos)
                return
            }

            if let dataString = String(data: data, encoding: String.Encoding.ascii) {
                for line in dataString.components(separatedBy: "\n") {

                    if let institution = parseLine(line: line) {
                        institutions.append(institution)
                    }
                }
            } else {
                errorDadosAbertos = ErrorDadosAbertos.tryToReach
            }

            completion(institutions, errorDadosAbertos)
        }

        task.resume()
    }

    /// Parse the string line of CSV to an Institution
    ///
    /// - Parameter line: A string representation of a line in CSV file
    /// - Returns: The Institution
    private static func parseLine(line: String) -> Institution? {

        var institution = Institution()
        let elements = line.components(separatedBy: ";")

        if elements.count < 12 {
            return nil
        }
        institution.informationYear = elements[0]
        institution.informationTrimester = elements[1]
        institution.category = elements[2]
        institution.type = elements[3]
        institution.CNPJ = elements[4]
        institution.name = elements[5]
        institution.index = Int(elements[6])
        institution.numberComplainsRegulatedProceeded = Int(elements[7])
        institution.numberComplainsRegulatedOthers = Int(elements[8])
        institution.numberTotalComplains = Int(elements[9])
        institution.numberTotalOfClients = Int(elements[10])
        institution.numberOfClientsCCS = Int(elements[11])
        institution.numberOfClientsSCR = Int(elements[12])
        institution.numberOfClientsFGC = Int(elements[13])

        return institution
    }
}
