//
//  TaxaSelicService.swift
//  DadosAbertosBCB
//
//  Created by Jessica Batista de Barros Cherque on 17/08/2018.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

/// Service to fetch the selic tax values in the BCB open data
public class SelicTaxService {

    /// URL Server
    private static var serverUrl = "https://api.bcb.gov.br"

    /// Daily SELIC tax API URL
    private static let endPoint = "/dados/serie/bcdata.sgs.11/dados?formato=json"

    /// Change the server for use webdservice
    public static func setServer(_ url: String) {
        serverUrl = url
    }

    /// Function that seeks the daily values of the selic tax
    ///
    /// - Parameter completion: function receiving the daily SELIC tax
    public static func allDailySelicTax(completion: @escaping ([DailySelicTax], ErrorDadosAbertos?) -> Void) {

        guard let url = URL(string: serverUrl + endPoint) else {
            completion([], ErrorDadosAbertos.tryToReach)
            return
        }

        let task = URLSession.shared.dataTask(with: url) { (data, _, error) in

            var selics: [DailySelicTax] = []

            guard let data = data, error == nil else { return }

            do {
                let newData = try JSONSerialization.jsonObject(with: data,
                                                               options: JSONSerialization.ReadingOptions.allowFragments)

                if let newData = newData as? [[String: Any]] {

                    for json in newData {

                        if let dateString = json["data"] as? String {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd/MM/yyyy"
                            if let date = dateFormatter.date(from: dateString) {
                                if let valueString = json["valor"] as? String {
                                    if let value = Float(valueString) {
                                        let taxa = DailySelicTax(date: date, value: value)
                                        selics.append(taxa)
                                    }
                                }
                            }
                        }
                    }
                    completion(selics, nil)
                }
            } catch {
                completion([], ErrorDadosAbertos.tryToReach)
            }
        }
        task.resume()
    }
}
