//
//  ErrorDadosAbertos.swift
//  DadosAbertosBCB
//
//  Created by Jessica Batista de Barros Cherque on 20/08/2018.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

public enum ErrorDadosAbertos {
    case tryToReach // "Got error when try to reach Dados Abertos Site"
    case tryToParseJson // "Got error when try to Deserializate JSON"
}
