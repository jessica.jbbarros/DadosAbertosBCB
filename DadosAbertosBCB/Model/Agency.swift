//
//  Agencia.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 20/08/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

/// Model of an Agency
public struct Agency {
    public var cnpjBase: String?
    public var cnpjSequencial: String?
    public var cnpjDv: String?
    public var nameIf: String?
    public var segment: String?
    public var codeCompe: String?
    public var agencyName: String?
    public var adress: String?
    public var number: String?
    public var complement: String?
    public var neightboord: String?
    public var cep: String?
    public var cityIbge: String?
    public var city: String?
    public var federativeUnit: String?
    public var initialDate: String?
    public var DDD: String?
    public var phone: String?
}
