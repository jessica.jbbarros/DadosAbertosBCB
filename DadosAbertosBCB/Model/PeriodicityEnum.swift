//
//  PeriodicityEnum.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 20/08/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

public enum Periodicity: String {
    case mensal = "MENSAL"
    case bimestral = "BIMESTRAL"
    case trimestral = "TRIMESTRAL"
    case semestral = "SEMESTRAL"
}
