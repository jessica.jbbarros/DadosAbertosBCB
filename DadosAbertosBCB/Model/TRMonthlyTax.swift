//
//  TRMonthlyTax.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 02/10/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

///  Monthly TR tax disclosed by the government
public struct MonthlyTRTax {
    public var date: Date?
    public var value: Float?
}
