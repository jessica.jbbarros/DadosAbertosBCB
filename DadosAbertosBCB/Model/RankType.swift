//
//  RankType.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 20/08/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

public enum RankType: String {
    case bancoEFinanceira = "Bancos+e+financeiras"
    case consorcio = "Consorcios"
}
