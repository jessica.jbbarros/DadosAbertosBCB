//
//  TaxaSelicDiaria.swift
//  DadosAbertosBCB
//
//  Created by Jessica Batista de Barros Cherque on 20/08/2018.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

///  Daily SELIC tax disclosed by the government
public struct DailySelicTax {
    public var date: Date?
    public var value: Float?
}
