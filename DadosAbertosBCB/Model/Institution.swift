//
//  Instituition.swift
//  DadosAbertosBCB
//
//  Created by Leonardo Alves de Melo on 20/08/18.
//  Copyright © 2018 tus. All rights reserved.
//

import Foundation

public struct Institution {
    public var informationYear: String?
    public var informationTrimester: String?
    public var category: String?
    public var type: String?
    public var CNPJ: String?
    public var name: String?
    public var index: Int?
    public var numberComplainsRegulatedProceeded: Int?
    public var numberComplainsRegulatedOthers: Int?
    public var numberTotalComplains: Int?
    public var numberTotalOfClients: Int?
    public var numberOfClientsCCS: Int?
    public var numberOfClientsSCR: Int?
    public var numberOfClientsFGC: Int?
}
