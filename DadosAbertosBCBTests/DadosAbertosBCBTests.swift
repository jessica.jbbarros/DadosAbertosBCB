//
//  DadosAbertosBCBTests.swift
//  DadosAbertosBCBTests
//
//  Created by Leonardo Alves de Melo on 20/08/18.
//  Copyright © 2018 tus. All rights reserved.
//

import XCTest
import DadosAbertosBCB

class DadosAbertosBCBTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    /// Test if it is getting correctly the Selic Data
    func testSelic() {

        let testExpectation = XCTestExpectation(description: "selic")

        SelicTaxService.allDailySelicTax { (selics, error)  in

            XCTAssertFalse(error != nil, "Erro ao executar")

            XCTAssertFalse(selics.count <= 0, "Não Temos Selics")

            for selic in selics {
                XCTAssertFalse(selic.date == nil, "Não Tem data")
                XCTAssertFalse(selic.value == nil, "Não Tem valor")
            }

            XCTAssert(true, "OK")
            testExpectation.fulfill()
        }

        self.wait(for: [testExpectation], timeout: 30.0)

    }

    /// Test if it is getting correctly the Agency Data
    func testAgency() {

        let testExpectation = XCTestExpectation(description: "agencia")

        SupervisedAgenciesService.getAgencies { (agencies, error) in

            XCTAssertFalse(error != nil, "Erro ao executar")
            XCTAssertFalse(agencies.count <= 0, "Não Temos agências")

            for agency in agencies {
                XCTAssertFalse(agency.cnpjBase == nil, "Não Tem cnpj")
            }

            XCTAssert(true, "OK")
            testExpectation.fulfill()
        }

        self.wait(for: [testExpectation], timeout: 30.0)
    }

    /// Test if it is getting correctly the Rank Data
    func testRank() {
        let testExpectation = XCTestExpectation(description: "rank")

        RankingInstitutionsService.getRanking(year: "2017", periodicity: .trimestral,
                                              period: 2, type: .bancoEFinanceira) { (institutions, error) in
            XCTAssertFalse(institutions.count <= 0, "Do not have institutions")
            XCTAssertFalse(error != nil, "Error")

            for institution in institutions {
                XCTAssertFalse(institution.name == nil)
            }

            XCTAssert(true, "OK")
            testExpectation.fulfill()
        }

        self.wait(for: [testExpectation], timeout: 30.0)
    }
}
